#!/usr/bin/env bash

# Install rust
#curl https://sh.rustup.rs -sSf | sh
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

# Install watchexec (via rust/cargo)
# cargo install watchexec-cli

